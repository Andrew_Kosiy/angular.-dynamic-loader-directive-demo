import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyDirectiveDirective } from './my-directive.directive';
import { LoaderComponent } from './loader/loader.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    MyDirectiveDirective,
    LoaderComponent
  ],
  imports: [
    CommonModule,
    BrowserModule
  ],
  entryComponents: [LoaderComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
