import {
  ComponentFactoryResolver, ComponentRef,
  Directive,
  EventEmitter,
  Input,
  OnChanges, OnDestroy,
  OnInit,
  Output,
  ViewContainerRef
} from '@angular/core';
import { LoaderComponent } from './loader/loader.component';

@Directive({
  selector: '[appMyDirective]'
})
export class MyDirectiveDirective implements OnInit, OnChanges, OnDestroy {
  @Input() loading: boolean;
  @Output() loaded: EventEmitter<any> = new EventEmitter();

  private loaderRef: ComponentRef<LoaderComponent>;

  constructor(private resolver: ComponentFactoryResolver, public container: ViewContainerRef) {}

  public ngOnInit(): void {
    const factory = this.resolver.resolveComponentFactory(LoaderComponent);
    this.loaderRef = this.container.createComponent(factory);
    this.loaderRef.instance.visible = this.loading;
  }

  public ngOnChanges(): void {
    if (!this.loaderRef) {
      return;
    }

    this.loaderRef.instance.visible = this.loading;

    if (!this.loading) {
      this.loaded.emit();
    }
  }

  public ngOnDestroy(): void {
    this.loaderRef.destroy();
  }
}
