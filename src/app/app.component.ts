import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Hello';

  public loading = true;

  public ngOnInit(): void {
    setTimeout(() => this.loading = false, 1000);
    setTimeout(() => this.loading = true, 2000);
    setTimeout(() => this.loading = false, 3000);
  }

  public loaded(): void {
    console.log('LOADED');
  }
}
